package com.braisblanco.oneGram;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class AGramm {
	
	public static void main(String args[]) throws IOException, ClassNotFoundException, InterruptedException {
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "a-gramm");
		job.setJarByClass(AGramm.class);
		job.setMapperClass(OneGramMapper.class);
		//job.setCombinerClass(OneGramReducer.class);
		job.setReducerClass(OneGramReducer.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
		FileInputFormat.addInputPath(job, new Path("/user/brais/input/googlebooks-spa-all-1gram-20120701-a"));
		//FileInputFormat.addInputPath(job, new Path("prueba.txt"));
		FileOutputFormat.setOutputPath(job, new Path("/user/brais/output_t1_p1"));
		
		System.out.println(job.waitForCompletion(true) ? "BIEN" : "ERROR");
		
		
	}
}
